﻿namespace PaisesDoMundo.Servicos
{
    using System;
    using System.Collections.Generic;
    using Modelos;
    using System.Data.SQLite;
    using System.IO;
    using System.Net;
    using Svg;
    using System.Drawing;

    class DataService
    {
        private SQLiteConnection connection;
        private SQLiteCommand command;
        private DialogService dialogService;
        private SvgDocument svg = new SvgDocument();
        private WebClient webClient = new WebClient();
        private HashSet<Currency> HSPaises = new HashSet<Currency>();
        private Bitmap bmp;
        private string DBpath = @"Data\PaisesDoMundoDB.sqlite";
        private string Imgpath = @"Images\";

        public DataService()
        {
            dialogService = new DialogService();

            if (!Directory.Exists("Data"))
            {
                Directory.CreateDirectory("Data");
            }
            if (!Directory.Exists("Images"))
            {
                Directory.CreateDirectory("Images");
            }

            connection = new SQLiteConnection("Data Source=" + DBpath);
            connection.Open();
            try
            {

                string sqlcommand = "CREATE TABLE IF NOT EXISTS country(Name VARCHAR(50), Capital VARCHAR(50), Region VARCHAR(50), Subregion VARCHAR(50), Population INT, Demonym VARCHAR(15), Area VARCHAR(20), NativeName VARCHAR(100), NumericCode VARCHAR(5), Cioc VARCHAR(5), CurrencyCode VARCHAR(5), CurrencyName VARCHAR(30), CurrencySymbol VARCHAR(5), Domain VARCHAR(5));";

                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                dialogService.ShowMessage("Erro - Países", ex.Message);
            }//Tabela Country
            connection.Close();
        }


        public List<Pais> GetData()
        {
            List<Pais> Paises = new List<Pais>();
            try
            {
                string sql = "select Name, Capital, Region, Subregion, Population, Currencies, TopLevelDomain, Flag";

                command = new SQLiteCommand(sql, connection);


                // LÊ CADA REGISTO
                SQLiteDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Paises.Add(new Pais
                    {
                        Name = (string)reader["name"],
                        Capital = (string)reader["capital"],
                        Region = (string)reader["region"],
                        Subregion = (string)reader["subregion"],
                        Population = (int)reader["population"],
                        Demonym = (string)reader["demonym"],
                        Area = (string)reader["area"],
                        NativeName = (string)reader["nativeName"],
                        Flag = (string)reader["flag"],
                        Cioc = (string)reader["cioc"],
                        Domain = (string)reader["domain"],
                        CurrencyCode = (string)reader["currencyCode"],
                        CurrencyName = (string)reader["currencyName"],
                        CurrencySymbol = (string)reader["currencySymbol"]
                    });
                }
                connection.Close();

                return Paises;
            }
            catch (Exception ex)
            {
                dialogService.ShowMessage("Erro", ex.Message);
                return null;
            }


        }


        public void SaveData(List<Pais> PaisesSrc) //INSERIR NO SQLITE
        {

            connection = new SQLiteConnection("Data Source=" + DBpath);
            connection.Open();

            //string sql = "";
            List<Pais> Paises = new List<Pais>();
            Paises = PaisesSrc;
            try
            {
                foreach (var pais in Paises)//TRANSFERIR LISTA PARA HASHSET
                {
                    //PUXAR IMAGEM SVG E CONVERTER PARA BMP
                    string path = Imgpath + pais.Name; //DESTINO DO FICHEIRO
                    if (!File.Exists(path + ".bmp"))
                    {
                        try
                        {
                            webClient.DownloadFile(pais.Flag, path + ".svg");   //DOWNLOAD DO SVG
                            webClient.Dispose();
                            svg = SvgDocument.Open(path + ".svg");
                            bmp = svg.Draw();
                            bmp.Save(path + ".bmp");
                            Image.FromFile(@"Images\" + pais.Name + ".bmp");
                            File.Delete(path + ".svg");
                        }
                        catch (Exception)
                        {
                        }
                    }

                    string sql = String.Format("INSERT INTO country(Name, Capital, Region, Subregion, Population, Demonym, Area, NativeName, NumericCode, Cioc, CurrencyCode, CurrencyName, CurrencySymbol, Domain) values (@Name, @Capital, @Region, @Subregion, @Population, @Demonym, @Area, @NativeName, @NumericCode, @Cioc, @CurrencyCode, @CurrencyName, @CurrencySymbol, @Domain)");


                    //(Name VARCHAR(50), Capital VARCHAR(50), Region VARCHAR(50), Subregion VARCHAR(50), Population INT, Demonym VARCHAR(15), Area VARCHAR(20), NativeName VARCHAR(100), NumericCode VARCHAR(5), Cioc VARCHAR(5), CurrencyCode VARCHAR(5), CurrencyName VARCHAR(30), CurrencySymbol VARCHAR(5), Domain VARCHAR(5))

                    command = new SQLiteCommand(sql, connection);

                    command.Parameters.AddWithValue("@Name", pais.Name);
                    command.Parameters.AddWithValue("@Capital", pais.Capital);
                    command.Parameters.AddWithValue("@Region", pais.Region);
                    command.Parameters.AddWithValue("@Subregion", pais.Subregion);
                    command.Parameters.AddWithValue("@Population", pais.Population);
                    command.Parameters.AddWithValue("@Demonym", pais.Demonym);
                    command.Parameters.AddWithValue("@Area", pais.Area);
                    command.Parameters.AddWithValue("@NativeName", pais.NativeName);
                    command.Parameters.AddWithValue("@NumericCode", pais.NumericCode);
                    command.Parameters.AddWithValue("@Cioc", pais.Cioc);
                    command.Parameters.AddWithValue("@CurrencyCode", pais.CurrencyCode);
                    command.Parameters.AddWithValue("@CurrencyName", pais.CurrencyName);
                    command.Parameters.AddWithValue("@CurrencySymbol", pais.CurrencySymbol);
                    command.Parameters.AddWithValue("@Domain", pais.Domain);

                    command.ExecuteScalar();

                }
            }
            catch (Exception ex)
            {
                dialogService.ShowMessage("Erro - Save Data", ex.Message);
            }//Inserir Países

            connection.Close();
        }


        public void DeleteData()
        {
            connection = new SQLiteConnection("Data Source=" + DBpath);
            connection.Open();

            try
            {
                string sql = "DELETE FROM country WHERE Population > -1";

                command = new SQLiteCommand(sql, connection);

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                dialogService.ShowMessage("Erro", ex.Message);
            }

            connection.Close();

        }

    }
}
