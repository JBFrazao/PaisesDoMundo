﻿namespace PaisesDoMundo.Servicos
{
    using Modelos;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;

    class ApiService
    {
        public async Task<Response> GetCountriesAsync(string UrlBase, string controller)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(UrlBase);
                var response = await client.GetAsync(controller);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = result
                    };
                }

                var paises = JsonConvert.DeserializeObject<List<Pais>>(result);
                
                

                return new Response
                {
                    IsSuccess = true,
                    Result = paises
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = true,
                    Message = ex.Message
                };
            }
        }
    }
}
