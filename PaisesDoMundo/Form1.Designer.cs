﻿namespace PaisesDoMundo
{
    partial class FormAtlas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PBLoad = new System.Windows.Forms.ProgressBar();
            this.CmbBoxPaises = new System.Windows.Forms.ComboBox();
            this.LblName = new System.Windows.Forms.Label();
            this.LblDomain = new System.Windows.Forms.Label();
            this.LblCapital = new System.Windows.Forms.Label();
            this.LblRegion = new System.Windows.Forms.Label();
            this.LblSubRegion = new System.Windows.Forms.Label();
            this.LblPopulation = new System.Windows.Forms.Label();
            this.LblDemonym = new System.Windows.Forms.Label();
            this.LblArea = new System.Windows.Forms.Label();
            this.LblNativeName = new System.Windows.Forms.Label();
            this.LblCioc = new System.Windows.Forms.Label();
            this.LblResultado = new System.Windows.Forms.Label();
            this.LblStatus = new System.Windows.Forms.Label();
            this.LblNumericCode = new System.Windows.Forms.Label();
            this.PicBoxFlag = new System.Windows.Forms.PictureBox();
            this.LblCCode = new System.Windows.Forms.Label();
            this.LblCName = new System.Windows.Forms.Label();
            this.LblCSymbol = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // PBLoad
            // 
            this.PBLoad.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PBLoad.Location = new System.Drawing.Point(0, 408);
            this.PBLoad.Name = "PBLoad";
            this.PBLoad.Size = new System.Drawing.Size(708, 10);
            this.PBLoad.TabIndex = 3;
            // 
            // CmbBoxPaises
            // 
            this.CmbBoxPaises.DropDownHeight = 1;
            this.CmbBoxPaises.DropDownWidth = 1;
            this.CmbBoxPaises.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmbBoxPaises.Font = new System.Drawing.Font("Cheque", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbBoxPaises.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.CmbBoxPaises.IntegralHeight = false;
            this.CmbBoxPaises.ItemHeight = 48;
            this.CmbBoxPaises.Location = new System.Drawing.Point(0, -65);
            this.CmbBoxPaises.Margin = new System.Windows.Forms.Padding(0);
            this.CmbBoxPaises.MaxDropDownItems = 1;
            this.CmbBoxPaises.Name = "CmbBoxPaises";
            this.CmbBoxPaises.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CmbBoxPaises.Size = new System.Drawing.Size(740, 56);
            this.CmbBoxPaises.TabIndex = 4;
            this.CmbBoxPaises.Visible = false;
            this.CmbBoxPaises.SelectedIndexChanged += new System.EventHandler(this.CmbBoxPaises_SelectedIndexChanged);
            // 
            // LblName
            // 
            this.LblName.AutoSize = true;
            this.LblName.BackColor = System.Drawing.Color.Transparent;
            this.LblName.Font = new System.Drawing.Font("Cheque", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblName.Location = new System.Drawing.Point(127, 293);
            this.LblName.Name = "LblName";
            this.LblName.Size = new System.Drawing.Size(120, 41);
            this.LblName.TabIndex = 5;
            this.LblName.Text = "Name";
            // 
            // LblDomain
            // 
            this.LblDomain.AutoSize = true;
            this.LblDomain.Location = new System.Drawing.Point(542, 153);
            this.LblDomain.Name = "LblDomain";
            this.LblDomain.Size = new System.Drawing.Size(43, 13);
            this.LblDomain.TabIndex = 6;
            this.LblDomain.Text = "Domain";
            // 
            // LblCapital
            // 
            this.LblCapital.AutoSize = true;
            this.LblCapital.Font = new System.Drawing.Font("Cheque", 20F);
            this.LblCapital.Location = new System.Drawing.Point(128, 353);
            this.LblCapital.Name = "LblCapital";
            this.LblCapital.Size = new System.Drawing.Size(126, 32);
            this.LblCapital.TabIndex = 10;
            this.LblCapital.Text = "Capital";
            // 
            // LblRegion
            // 
            this.LblRegion.AutoSize = true;
            this.LblRegion.Location = new System.Drawing.Point(542, 101);
            this.LblRegion.Name = "LblRegion";
            this.LblRegion.Size = new System.Drawing.Size(41, 13);
            this.LblRegion.TabIndex = 12;
            this.LblRegion.Text = "Region";
            // 
            // LblSubRegion
            // 
            this.LblSubRegion.AutoSize = true;
            this.LblSubRegion.Location = new System.Drawing.Point(542, 114);
            this.LblSubRegion.Name = "LblSubRegion";
            this.LblSubRegion.Size = new System.Drawing.Size(60, 13);
            this.LblSubRegion.TabIndex = 13;
            this.LblSubRegion.Text = "SubRegion";
            // 
            // LblPopulation
            // 
            this.LblPopulation.AutoSize = true;
            this.LblPopulation.Location = new System.Drawing.Point(542, 127);
            this.LblPopulation.Name = "LblPopulation";
            this.LblPopulation.Size = new System.Drawing.Size(57, 13);
            this.LblPopulation.TabIndex = 14;
            this.LblPopulation.Text = "Population";
            // 
            // LblDemonym
            // 
            this.LblDemonym.AutoSize = true;
            this.LblDemonym.Location = new System.Drawing.Point(542, 140);
            this.LblDemonym.Name = "LblDemonym";
            this.LblDemonym.Size = new System.Drawing.Size(54, 13);
            this.LblDemonym.TabIndex = 16;
            this.LblDemonym.Text = "Demonym";
            // 
            // LblArea
            // 
            this.LblArea.AutoSize = true;
            this.LblArea.Font = new System.Drawing.Font("Cheque", 15F);
            this.LblArea.Location = new System.Drawing.Point(284, 360);
            this.LblArea.Name = "LblArea";
            this.LblArea.Size = new System.Drawing.Size(64, 24);
            this.LblArea.TabIndex = 17;
            this.LblArea.Text = "Area";
            // 
            // LblNativeName
            // 
            this.LblNativeName.AutoSize = true;
            this.LblNativeName.BackColor = System.Drawing.Color.Transparent;
            this.LblNativeName.Font = new System.Drawing.Font("Cheque Black", 15F);
            this.LblNativeName.Location = new System.Drawing.Point(130, 334);
            this.LblNativeName.Name = "LblNativeName";
            this.LblNativeName.Size = new System.Drawing.Size(148, 24);
            this.LblNativeName.TabIndex = 21;
            this.LblNativeName.Text = "Native Name";
            // 
            // LblCioc
            // 
            this.LblCioc.AutoSize = true;
            this.LblCioc.Location = new System.Drawing.Point(542, 88);
            this.LblCioc.Name = "LblCioc";
            this.LblCioc.Size = new System.Drawing.Size(28, 13);
            this.LblCioc.TabIndex = 28;
            this.LblCioc.Text = "Cioc";
            // 
            // LblResultado
            // 
            this.LblResultado.AutoSize = true;
            this.LblResultado.Location = new System.Drawing.Point(12, 392);
            this.LblResultado.Name = "LblResultado";
            this.LblResultado.Size = new System.Drawing.Size(35, 13);
            this.LblResultado.TabIndex = 1;
            this.LblResultado.Text = "label1";
            // 
            // LblStatus
            // 
            this.LblStatus.AutoSize = true;
            this.LblStatus.Location = new System.Drawing.Point(356, 392);
            this.LblStatus.Name = "LblStatus";
            this.LblStatus.Size = new System.Drawing.Size(0, 13);
            this.LblStatus.TabIndex = 2;
            // 
            // LblNumericCode
            // 
            this.LblNumericCode.AutoSize = true;
            this.LblNumericCode.Font = new System.Drawing.Font("Cheque Black", 40F);
            this.LblNumericCode.Location = new System.Drawing.Point(12, 312);
            this.LblNumericCode.Name = "LblNumericCode";
            this.LblNumericCode.Size = new System.Drawing.Size(118, 65);
            this.LblNumericCode.TabIndex = 45;
            this.LblNumericCode.Text = "000";
            // 
            // PicBoxFlag
            // 
            this.PicBoxFlag.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PicBoxFlag.Location = new System.Drawing.Point(0, 0);
            this.PicBoxFlag.Name = "PicBoxFlag";
            this.PicBoxFlag.Size = new System.Drawing.Size(536, 297);
            this.PicBoxFlag.TabIndex = 46;
            this.PicBoxFlag.TabStop = false;
            // 
            // LblCCode
            // 
            this.LblCCode.AutoSize = true;
            this.LblCCode.Location = new System.Drawing.Point(542, 166);
            this.LblCCode.Name = "LblCCode";
            this.LblCCode.Size = new System.Drawing.Size(77, 13);
            this.LblCCode.TabIndex = 47;
            this.LblCCode.Text = "Currency Code";
            // 
            // LblCName
            // 
            this.LblCName.AutoSize = true;
            this.LblCName.Location = new System.Drawing.Point(542, 179);
            this.LblCName.Name = "LblCName";
            this.LblCName.Size = new System.Drawing.Size(80, 13);
            this.LblCName.TabIndex = 48;
            this.LblCName.Text = "Currency Name";
            // 
            // LblCSymbol
            // 
            this.LblCSymbol.AutoSize = true;
            this.LblCSymbol.Location = new System.Drawing.Point(542, 192);
            this.LblCSymbol.Name = "LblCSymbol";
            this.LblCSymbol.Size = new System.Drawing.Size(86, 13);
            this.LblCSymbol.TabIndex = 49;
            this.LblCSymbol.Text = "Currency Symbol";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 290);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(736, 128);
            this.pictureBox1.TabIndex = 50;
            this.pictureBox1.TabStop = false;
            // 
            // FormAtlas
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(708, 418);
            this.Controls.Add(this.LblStatus);
            this.Controls.Add(this.LblResultado);
            this.Controls.Add(this.PBLoad);
            this.Controls.Add(this.LblNumericCode);
            this.Controls.Add(this.LblNativeName);
            this.Controls.Add(this.LblArea);
            this.Controls.Add(this.LblCapital);
            this.Controls.Add(this.LblName);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.LblCSymbol);
            this.Controls.Add(this.LblCName);
            this.Controls.Add(this.LblCCode);
            this.Controls.Add(this.LblCioc);
            this.Controls.Add(this.LblDemonym);
            this.Controls.Add(this.LblPopulation);
            this.Controls.Add(this.LblSubRegion);
            this.Controls.Add(this.LblRegion);
            this.Controls.Add(this.LblDomain);
            this.Controls.Add(this.CmbBoxPaises);
            this.Controls.Add(this.PicBoxFlag);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.ImeMode = System.Windows.Forms.ImeMode.HangulFull;
            this.Name = "FormAtlas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Atlas";
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ProgressBar PBLoad;
        private System.Windows.Forms.Label LblName;
        private System.Windows.Forms.Label LblDomain;
        private System.Windows.Forms.Label LblCapital;
        private System.Windows.Forms.Label LblRegion;
        private System.Windows.Forms.Label LblSubRegion;
        private System.Windows.Forms.Label LblPopulation;
        private System.Windows.Forms.Label LblDemonym;
        private System.Windows.Forms.Label LblArea;
        private System.Windows.Forms.Label LblNativeName;
        private System.Windows.Forms.Label LblCioc;
        private System.Windows.Forms.Label LblResultado;
        private System.Windows.Forms.Label LblStatus;
        private System.Windows.Forms.ComboBox CmbBoxPaises;
        private System.Windows.Forms.Label LblNumericCode;
        private System.Windows.Forms.PictureBox PicBoxFlag;
        private System.Windows.Forms.Label LblCCode;
        private System.Windows.Forms.Label LblCName;
        private System.Windows.Forms.Label LblCSymbol;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

