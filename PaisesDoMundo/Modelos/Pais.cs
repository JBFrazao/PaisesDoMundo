﻿namespace PaisesDoMundo.Modelos
{
    using System.Collections.Generic;

    class Pais
    {
        public string Name { get; set; }
        public string Capital { get; set; }
        public string Region { get; set; }
        public string Subregion { get; set; }
        public int Population { get; set; }
        public string Demonym { get; set; }
        public string Area { get; set; }
        public string NativeName { get; set; }
        public string NumericCode { get; set; }
        public string Flag { get; set; }
        public string Cioc { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencySymbol { get; set; }
        public string Domain { get; set; }
        public List<Currency> Currencies { get; set; }
        public List<string> TopLevelDomain { get; set; }


        public void SetCurrency()
        {
            if (Currencies.Count>0)
            {
                CurrencyCode = Currencies[0].Code;
                CurrencyName = Currencies[0].Name;
                CurrencySymbol = Currencies[0].Symbol;
            }
        }

        public void SetDomain()
        {
            if (TopLevelDomain.Count > 0)
            {
                Domain = TopLevelDomain[0];
            }
        }

    }
}
