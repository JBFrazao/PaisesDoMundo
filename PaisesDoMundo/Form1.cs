﻿namespace PaisesDoMundo
{
    using Servicos;
    using System.Collections.Generic;
    using Modelos;
    using System.Windows.Forms;
    using System;
    using System.Threading.Tasks;
    using System.IO;
    using System.Drawing;

    public partial class FormAtlas : Form
    {
        #region Atributos
        private List<Pais> Paises;
        private NetworkService networkService;
        private ApiService apiService;
        private DialogService dialogService;
        private DataService dataService;
        #endregion

        public FormAtlas()
        {
            InitializeComponent();
            Paises = new List<Pais>();
            networkService = new NetworkService();
            apiService = new ApiService();
            dialogService = new DialogService();
            dataService = new DataService();
            LoadPaises();
        }

        private async void LoadPaises()
        {
            bool load;

            LblResultado.Text = "A atualizar taxas...";

            var connection = networkService.CheckConnection();
            if (!connection.IsSuccess)
            {
                //LoadLocalRates();
                load = false;
            }
            else
            {
                await LoadApiPaises();
                load = true;
            }

            if (Paises.Count == 0)
            {
                LblResultado.Text = "Não há ligação à Internet" + Environment.NewLine + "e não foram previamente carregadas as taxas." + Environment.NewLine + "Tente mais tarde!";
                LblResultado.Text = "Primeira inicialização deverá ter ligação à Internet";
                return;
            }

            CmbBoxPaises.DataSource = Paises;
            CmbBoxPaises.DisplayMember = "Name";




            LblResultado.Text = "Taxas carregadas!";

            if (load)
            {
                LblStatus.Text = string.Format("Taxas carregadas da Internet no dia {0:F}", DateTime.Now);
            }
            else
            {
                LblStatus.Text = string.Format("Taxas carregadas da base de dados local");
            }

            PBLoad.Value = 100;
            CmbBoxPaises.Visible = true;

        }

        private async Task LoadApiPaises()
        {
            PBLoad.Value = 0;

            var response = await apiService.GetCountriesAsync("https://restcountries.eu", "rest/v2/all");

           

            Paises = (List<Pais>)response.Result;

            foreach (var item in Paises)//PASSAR OS REGISTOS DA LISTA PARA PROPRIEDADES STRING NA CLASSE PAIS
            {
                item.SetCurrency();
                item.SetDomain();
            }

            //dataService.DeleteData(); //APAGAR A INFORMAÇÃO NA BD
            //dataService.SaveData(Paises); //GUARDAR NA BD

        }

        private void CmbBoxPaises_SelectedIndexChanged(object sender, EventArgs e)
        {
            AtualizaDados();
        } //ATUALIZAR DADOS QUANDO O INDICE DA COMBOBOX MUDA

        private void AtualizaDados()
        {
            Pais pais = Paises[CmbBoxPaises.SelectedIndex];

            LblName.Text = pais.Name;   
            LblCapital.Text = pais.Capital;
            LblRegion.Text = "Region | " + pais.Region;
            LblSubRegion.Text = "Sub-Region | " + pais.Subregion;
            LblPopulation.Text = "Population | " + pais.Population.ToString();
            LblDemonym.Text = "Demonym | " + pais.Demonym;
            LblArea.Text = "Area | " + pais.Area;
            LblNativeName.Text = "Native Name | " + pais.NativeName;
            LblCCode.Text = pais.CurrencyCode;
            LblCName.Text = pais.CurrencyName;
            LblCSymbol.Text = pais.CurrencySymbol;
            LblDomain.Text = "Domain | " + pais.Domain;
            LblCioc.Text = "Cioc | " + pais.Cioc;

            if (pais.NumericCode == null)
            {
                LblNumericCode.Text = "????";
            }
            else
            {
                LblNumericCode.Text = pais.NumericCode.ToString();
            }
            if (File.Exists(@"Images\" + pais.Name + ".bmp"))
            {
                Image img = Image.FromFile(@"Images\" + pais.Name + ".bmp");
                FormAtlas FA = new FormAtlas();
                PicBoxFlag.Image = img;
                PicBoxFlag.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        } //ACTUALIZA AS LABELS
    }
}
